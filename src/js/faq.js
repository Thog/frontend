import { getInvite } from "./inviteHello.js";

getInvite().then(
  invite => (document.getElementById("faq-invite").href = invite)
);
