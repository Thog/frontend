import onLoad from "./onload";

export async function getInvite() {
  if (!window.invitePromise)
    window.invitePromise =
      localStorage.invite && localStorage.inviteExpires > Date.now()
        ? Promise.resolve(localStorage.invite)
        : // SSR whenplz
          (window.client
            ? Promise.resolve(window.client)
            : new Promise((resolve, reject) => {
                window.addEventListener("clientLoad", function() {
                  resolve(window.client);
                });
              })
          )
            .then(() =>
              window.client.getHello().then(hello => {
                localStorage.inviteExpires = Date.now() + 86400000;
                localStorage.invite = hello.invite;
              })
            )
            .catch(err => Promise.reject(err));
  const invite = await window.invitePromise;
  await new Promise(res => onLoad(() => res()));
  return invite;
}
