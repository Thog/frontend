function wrapPromise(end) {
  if (window.client) return end();
  else {
    return new Promise(resolve => {
      window.addEventListener("clientLoad", () => resolve());
    }).then(() => end());
  }
}

export default wrapPromise;
