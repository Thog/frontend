import onLoad from "./onload.js";

onLoad(function() {
  const resetBtn = document.getElementById("change-btn");
  const password = document.getElementById("password");
  const passwordValidity = document.getElementById("password-validity");
  const resetForm = document.getElementById("reset-form");
  resetForm.addEventListener("submit", function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    return false;
  });
  resetBtn.addEventListener("click", async function() {
    try {
      await client.confirmPasswordReset(
        window.location.hash.substring(1),
        password.value
      );
      window.location = "/";
    } catch (err) {
      passwordValidity.innerText = err.userMessage || err.message;
      password.setCustomValidity(err.userMessage || err.message);
      resetForm.classList = "was-validated needs-validation";
    }
  });
});
